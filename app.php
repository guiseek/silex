<?php
require_once __DIR__.'/bootstrap.php';

$app = new Silex\Application();

use Symfony\Component\HttpFoundation\Response;

//configuration
$app->register(new Silex\Provider\SessionServiceProvider());

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

$app->register(new Silex\Provider\SwiftmailerServiceProvider());

$app->register(new Silex\Provider\SessionServiceProvider());

//actions
$app->get('/', function ()  use ($app, $em) {
	$q = $em->createQuery("select u from Seek\Model\User u");
    $users = $q->getResult();
	
    return $app['twig']->render('user.twig', array(
        'users' => $users
	));
});
// Actions REST
$app->get('/rest/users', function ()  use ($app, $em) {
    $q = $em->createQuery("select u from Seek\Model\User u");
    $users = $q->getArrayResult();

    return new Response(json_encode($users), 200, array('Content-Type' => 'application/json'));
});

$app->get('/rest/user/{id}', function ($id)  use ($app, $em) {
    $q = $em->createQuery("select u from Seek\Model\User u where u.id = $id");
    $user = $q->getArrayResult();

    return new Response(json_encode($user), 200, array('Content-Type' => 'application/json'));
});


$app->post('/user', function() use ($app, $em) {
    $name = $app['request']->get('name');
    $username = $app['request']->get('username');
    $password = $app['request']->get('password');
    $email = $app['request']->get('email');
    $user = $em->getRepository('Seek\Model\User')->findBy(array('username' => $username));
    if (count($user) == 0) {
        $user = new Seek\Model\User();
        $user->setName($name);
        $user->setUsername($username);
        $user->setPassword($password);
        $user->setEmail($email);

        $em->persist($user);
        $em->flush();
        return $app->redirect('/');
    }
    return $app['twig']->render('message.twig', array(
        'message' => 'User exists'
    ));
});

$app->get('/login', function ()  use ($app, $em) {
    return $app['twig']->render('login.twig');
});

$app->post('/login', function () use ($app, $em) {
    $username = $app['request']->get('username');
    $password = $app['request']->get('password');

    $user = $em->getRepository('Seek\Model\User')->findBy(array('username' => $username, 'password' => sha1($password)));

    if (count($user) === 1) {
        $app['session']->set('user', array('username' => $username));
        return $app->redirect('/account');
    }

    $response = new Response();
    $response->headers->set('WWW-Authenticate', sprintf('Basic realm="%s"', 'site_login'));
    $response->setStatusCode(401, 'Please sign in.');
    return $response;
});

$app->get('/logout', function () use ($app) {
    $app['session']->clear();
    $app['session']->setFlash('msg', 'logged out.');
    return $app->redirect('/login');
});

$app->get('/account', function () use ($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('/login');
    }

    return "Welcome {$user['username']}!";
});